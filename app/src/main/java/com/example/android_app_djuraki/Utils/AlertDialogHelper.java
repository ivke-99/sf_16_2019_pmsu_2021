package com.example.android_app_djuraki.Utils;
import android.content.Context;


import androidx.appcompat.app.AlertDialog;


public class AlertDialogHelper {

    public static void ShowAlert(Context context,String title,String message)
    {
        AlertDialog.Builder m = new AlertDialog.Builder(context);
        m.setCancelable(false);
        m.setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).setTitle(title).setMessage(message).show();
    }
}
