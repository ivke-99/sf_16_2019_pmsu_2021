package com.example.android_app_djuraki.CustomerActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.adapters.CreateCommentDTO;
import com.example.android_app_djuraki.R;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class CustomerRateOrderActivity extends AppCompatActivity {

    Button postComment;
    TextInputLayout CommentText, Rating;
    String commentText;
    Integer rating = 0;
    RadioButton isAnonymous;
    String salesmanID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_rate_order);
        setTitle("Rate order");
        postComment = findViewById(R.id.postComment);
        CommentText = findViewById(R.id.customer_order_comment_text);
        Rating = findViewById(R.id.customer_order_comment_rating);
        isAnonymous = findViewById(R.id.customer_comment_isAnonymous_radio);
        String orderID = getIntent().getStringExtra("orderID");
        salesmanID = getIntent().getStringExtra("salesmanID");
        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        postComment.setOnClickListener(v -> {
            commentText = CommentText.getEditText().getText().toString().trim();
            if(!TextUtils.isEmpty(Rating.getEditText().getText())) {
                rating = Integer.parseInt(Rating.getEditText().getText().toString().trim());
            }
            else{
                rating = 0;
            }
            if (isValid()) {
                String key = FirebaseDatabase.getInstance().getReference("salesman").child(salesmanID).child("comments").push().getKey();
                CreateCommentDTO createCommentDTO = new CreateCommentDTO(rating, commentText, isAnonymous.isChecked(), false, userID);
                FirebaseDatabase.getInstance().getReference("salesman").child(salesmanID).child("comments").child(key).setValue(createCommentDTO).addOnSuccessListener(unused -> {
                    FirebaseDatabase.getInstance().getReference("customer").child(userID).child("orders").child(orderID).child("isCommented").setValue(true).addOnSuccessListener(unused1 -> {
                        FirebaseDatabase.getInstance().getReference("salesman").child(salesmanID).child("comments").get().addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                if (task.getResult().hasChildren()) {
                                    ArrayList<Integer> ratings = new ArrayList<>();
                                    for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                        ratings.add(snapshot.child("rating").getValue(Integer.class));
                                    }
                                    Double average = 0.00;
                                    for (Integer rating : ratings) {
                                        average += rating;
                                    }
                                    average = average / ratings.size();
                                    average = round(average);
                                    FirebaseDatabase.getInstance().getReference("salesman").child(salesmanID).child("rating").setValue(average).addOnCompleteListener(task1 -> {
                                        Toast.makeText(this, "Thank you for your feedback!", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(this, CustomerMainPage.class);
                                        startActivity(i);
                                    });
                                }
                            }
                        });
                    });
                });
            }
        });
    }

    private static double round(double value) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    private boolean isValid() {
        CommentText.setErrorEnabled(false);
        CommentText.setError("");
        Rating.setErrorEnabled(false);
        Rating.setError("");

        boolean isValidComment = false, isValidRating = false;
        if (TextUtils.isEmpty(commentText)) {
            CommentText.setErrorEnabled(true);
            CommentText.setError("Comment text cannot be empty.");
        } else {
            isValidComment = true;
        }
        if (rating == 0) {
            Rating.setErrorEnabled(false);
            Rating.setError("Rating must be from 1 to 5.");
        } else {
            isValidRating = true;
        }
        return isValidComment && isValidRating;
    }
}