package com.example.android_app_djuraki.CustomerActivities;

import android.content.Intent;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.CustomerHomeFragment;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.CustomerItemsFragment;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.CustomerOrdersFragment;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.GlobalActivities.MainActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.seismic.ShakeDetector;

import java.util.HashMap;
import java.util.Random;

import lombok.var;

public class CustomerMainPage extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    ShakeDetector sd;
    SensorManager sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_main_page);
        BottomNavigationView navigationView = findViewById(R.id.customer_bottom_navigation);
        loadCustomerFragment(new CustomerHomeFragment());
        navigationView.setOnNavigationItemSelectedListener(this);

        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sd = new ShakeDetector(() -> {
            int song = randomizePlayer();
            MediaPlayer mediaPlayer = MediaPlayer.create(this,song);
            sd.stop();
            mediaPlayer.start();
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                mediaPlayer.stop();
                sd.start(sm);
            }, 15000);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        sd.start(sm);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sd.stop();
    }

    public int randomizePlayer() {
        int random = (int) (Math.random() * 7 + 1);
        switch (random) {
            case 1:
                return R.raw.backinblack;
            case 2:
                return R.raw.corazonespinado;
            case 3:
                return R.raw.crazytrain;
            case 4:
                return R.raw.masterofpuppets;
            case 5:
                return R.raw.megustas;
            case 6:
                return R.raw.sweethomealabama;
            case 7:
                return R.raw.trappedunderice;
            default:
                throw new IllegalStateException("Unexpected value: " + random);
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.customer_home:
                fragment = new CustomerHomeFragment();
                break;
            case R.id.customer_order_items:
                fragment = new CustomerItemsFragment();
                break;
            case R.id.customer_view_orders:
                fragment = new CustomerOrdersFragment();
                break;
        }
        return loadCustomerFragment(fragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation_top_admin, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        return true;
    }

    private boolean loadCustomerFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
            return true;
        }
        return false;
    }
}
