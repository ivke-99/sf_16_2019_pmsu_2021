package com.example.android_app_djuraki.CustomerActivities.customerPanel.adapters;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OrderCreateDTO {
    private String salesmanID;
    private String dateOfOrder;
    private String orderedItems;
    private Boolean isDelivered;
    private Boolean isCommented;
}
