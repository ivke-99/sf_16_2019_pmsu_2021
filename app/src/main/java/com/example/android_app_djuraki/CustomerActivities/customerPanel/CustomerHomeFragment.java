package com.example.android_app_djuraki.CustomerActivities.customerPanel;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.adapters.CustomerProfileDTO;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.adapters.SalesmanProfileDTO;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class CustomerHomeFragment extends Fragment {

    TextView Email,FullName,Address;
    private ProgressDialog progressDialog = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_customer_home, null);
        Objects.requireNonNull(getActivity()).setTitle("Home");
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        if(view != null) {
            Email = view.findViewById(R.id.email_field_customer);
            FullName = view.findViewById(R.id.fullName_customer_field);
            Address = view.findViewById(R.id.address_customer_field);
            getUserInfo();
        }
    }

    private void getUserInfo() {

        progressDialog.show();
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("customer").child(userId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                CustomerProfileDTO customerProfileDTO = snapshot.getValue(CustomerProfileDTO.class);
                Email.setText(customerProfileDTO.getUsername());
                FullName.setText(String.format("%s %s", customerProfileDTO.getFName(), customerProfileDTO.getLName()));
                Address.setText(customerProfileDTO.getAddress());
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
