package com.example.android_app_djuraki.CustomerActivities.customerPanel.commentView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.CommentModelAdapter;

import java.util.List;

public class CommentViewModelAdapter extends RecyclerView.Adapter<CommentViewModelAdapter.ViewHolder> {

    private final Context mContext;
    private final List<CommentModelAdapter> customerCommentAdapterLIst;

    public CommentViewModelAdapter(Context mContext, List<CommentModelAdapter> customerCommentAdapterLIst) {
        this.mContext = mContext;
        this.customerCommentAdapterLIst = customerCommentAdapterLIst;
    }

    @NonNull
    @Override
    public CommentViewModelAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.customer_menu_comment,parent,false);
        return new CommentViewModelAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewModelAdapter.ViewHolder holder, int position) {
        final CommentModelAdapter adapter = customerCommentAdapterLIst.get(position);
            if (adapter.getIsAnonymous()) {
                holder.User.setText("Anonymous comment");
            } else {
                holder.User.setText(String.format("User: %s", adapter.getUserName()));
            }
            holder.CommentText.setText(String.format("Comment text: %s", adapter.getCommentText()));
            holder.Rating.setText(String.format("Rating: %s",adapter.getRating()));
    }

    @Override
    public int getItemCount() {
        return customerCommentAdapterLIst.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView User,Rating,CommentText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            User = itemView.findViewById(R.id.customer_comment_user);
            Rating = itemView.findViewById(R.id.customer_comment_rating);
            CommentText = itemView.findViewById(R.id.customer_comment_text);
        }
    }
}
