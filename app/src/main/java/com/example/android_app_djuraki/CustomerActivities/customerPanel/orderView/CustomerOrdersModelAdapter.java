package com.example.android_app_djuraki.CustomerActivities.customerPanel.orderView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.CustomerActivities.CustomerRateOrderActivity;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView.ShopModelAdapter;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.SalesmanEditDeleteItemActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class CustomerOrdersModelAdapter extends RecyclerView.Adapter<CustomerOrdersModelAdapter.ViewHolder> {

    private final Context mContext;
    private final List<CustomerOrderModelAdapter> customerOrderModelAdapterList;

    public CustomerOrdersModelAdapter(Context mContext, List<CustomerOrderModelAdapter> customerOrderModelAdapterList) {
        this.mContext = mContext;
        this.customerOrderModelAdapterList = customerOrderModelAdapterList;
    }


    @NonNull
    @Override
    public CustomerOrdersModelAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.customer_menu_order, parent, false);
        return new CustomerOrdersModelAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerOrdersModelAdapter.ViewHolder holder, int position) {
        final CustomerOrderModelAdapter customerOrderModelAdapter = customerOrderModelAdapterList.get(position);
            holder.CompanyName.setText(customerOrderModelAdapter.getCompanyName());
            holder.DateOfOrder.setText(customerOrderModelAdapter.getDateOfOrder().toString());
            holder.IsDelivered.setText(String.format("Is delivered:%s", customerOrderModelAdapter.getIsDelivered().toString()));
            holder.Items.setText(String.format("Items: %s", customerOrderModelAdapter.getOrderedItems()));
            if (!customerOrderModelAdapter.getIsDelivered()) {
                holder.btnLeaveRating.setVisibility(View.GONE);
                holder.btnLeaveRating.setEnabled(false);
            }
            else if(!customerOrderModelAdapter.getIsCommented()){
                holder.btnLeaveRating.setVisibility(View.VISIBLE);
                holder.btnLeaveRating.setEnabled(true);
                holder.btnMarkDelivery.setVisibility(View.GONE);
                holder.btnMarkDelivery.setEnabled(false);
            }
            else if(customerOrderModelAdapter.getIsCommented() && customerOrderModelAdapter.getIsDelivered()) {
                holder.btnLeaveRating.setVisibility(View.GONE);
                holder.btnLeaveRating.setEnabled(false);
                holder.btnMarkDelivery.setVisibility(View.GONE);
                holder.btnMarkDelivery.setEnabled(false);
            }

            holder.btnMarkDelivery.setOnClickListener(v -> {
                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                customerOrderModelAdapterList.remove(customerOrderModelAdapter);
                customerOrderModelAdapter.setIsDelivered(true);
                customerOrderModelAdapterList.add(customerOrderModelAdapter);
                FirebaseDatabase.getInstance().getReference("customer").child(userId).child("orders").child(customerOrderModelAdapter.getOrderID())
                        .child("isDelivered").setValue(true).addOnSuccessListener(unused -> Toast.makeText(mContext, "Thank you for your feedback!", Toast.LENGTH_LONG).show());
                notifyDataSetChanged();
            });

            holder.btnLeaveRating.setOnClickListener(v -> {
                Intent i = new Intent(mContext, CustomerRateOrderActivity.class);
                i.putExtra("salesmanID", customerOrderModelAdapter.getSalesmanID());
                i.putExtra("orderID", customerOrderModelAdapter.getOrderID());
                mContext.startActivity(i);
            });
    }

    @Override
    public int getItemCount() {
        return customerOrderModelAdapterList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView CompanyName, IsDelivered, DateOfOrder, Items;
        Button btnLeaveRating,btnMarkDelivery;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            CompanyName = itemView.findViewById(R.id.customer_order_salesman);
            IsDelivered = itemView.findViewById(R.id.customer_order_isDelivered);
            Items = itemView.findViewById(R.id.customer_order_itemList);
            DateOfOrder = itemView.findViewById(R.id.customer_order_date);
            btnLeaveRating = itemView.findViewById(R.id.button_customer_leave_rating);
            btnMarkDelivery = itemView.findViewById(R.id.button_customer_delivered_order);
        }
    }
}
