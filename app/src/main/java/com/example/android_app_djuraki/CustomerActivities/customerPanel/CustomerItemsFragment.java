package com.example.android_app_djuraki.CustomerActivities.customerPanel;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android_app_djuraki.AdminActivities.userView.AdminUsersAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.DishCustomerModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView.SalesmanModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView.ShopModelAdapter;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.CommentModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.SalesmanCommentsAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;
import com.example.android_app_djuraki.dao.FirebaseCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import lombok.Data;
import lombok.var;


public class CustomerItemsFragment extends Fragment {

    RecyclerView recyclerView;
    private final List<SalesmanModelAdapter> salesmanModelAdapterList = new ArrayList<>();
    private ProgressDialog progressDialog = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_customer_items, null);
        getActivity().setTitle("Companies");
        recyclerView = v.findViewById(R.id.recycle_menu_customer_companies);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getData();
        return v;
    }

    private void getData() {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
        FirebaseDatabase.getInstance().getReference("salesman").addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                salesmanModelAdapterList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                    SalesmanModelAdapter salesmanModelAdapter = new SalesmanModelAdapter();
                    salesmanModelAdapter.setId(snapshot1.getKey());
                    salesmanModelAdapter.setCompanyName(snapshot1.child("companyName").getValue(String.class));
                    salesmanModelAdapter.setRating(snapshot1.child("rating").getValue(Double.class));
                    ArrayList<DishCustomerModelAdapter> dishes = new ArrayList<>();
                    for(DataSnapshot snapshot2 : snapshot1.child("items").getChildren()) {
                        DishCustomerModelAdapter d1 = new DishCustomerModelAdapter();
                        d1.setId(snapshot2.getKey());
                        DishModelAdapter dishModelAdapter = new DishModelAdapter();
                        dishModelAdapter.setDish(snapshot2.child("dish").getValue(String.class));
                        dishModelAdapter.setDescription(snapshot2.child("description").getValue(String.class));
                        dishModelAdapter.setPrice(snapshot2.child("price").getValue(Double.class));
                        d1.setDish(dishModelAdapter);
                        d1.setDesiredQuantity(0);
                        dishes.add(d1);
                    }
                    ArrayList<CommentModelAdapter> comments = new ArrayList<>();
                    for(DataSnapshot snapshot2: snapshot1.child("comments").getChildren()) {
                        if (!snapshot2.child("isArchived").getValue(Boolean.class)) {
                            CommentModelAdapter commentModelAdapter = new CommentModelAdapter();
                            commentModelAdapter.setCommentText(snapshot2.child("commentText").getValue(String.class));
                            commentModelAdapter.setIsAnonymous(snapshot2.child("isAnonymous").getValue(Boolean.class));
                            commentModelAdapter.setIsArchived(snapshot2.child("isArchived").getValue(Boolean.class));
                            commentModelAdapter.setRating(snapshot2.child("rating").getValue(Integer.class));
                            String userId = snapshot2.child("userID").getValue(String.class);
                            FirebaseDatabase.getInstance().getReference("customer").child(userId).child("username").get().addOnCompleteListener(task -> {
                                if (task.isComplete()) {
                                    commentModelAdapter.setUserName(task.getResult().getValue(String.class));
                                    comments.add(commentModelAdapter);
                                }
                            });
                        }
                    }
                    salesmanModelAdapter.setCompanyProducts(dishes);
                    salesmanModelAdapter.setSalesmanComments(comments);
                    salesmanModelAdapterList.add(salesmanModelAdapter);
                    ShopModelAdapter shop = new ShopModelAdapter(getContext(), salesmanModelAdapterList);
                    recyclerView.setAdapter(shop);
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }
}