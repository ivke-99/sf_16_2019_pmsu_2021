package com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.DishCustomerModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.CommentModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SalesmanModelAdapter implements Serializable {
    private String id;
    private String companyName;
    private ArrayList<DishCustomerModelAdapter> companyProducts;
    private Double rating = 0.00;
    private ArrayList<CommentModelAdapter> salesmanComments;
}
