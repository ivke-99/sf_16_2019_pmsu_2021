package com.example.android_app_djuraki.CustomerActivities.customerPanel;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.DishCustomerModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.orderView.CustomerOrderModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.orderView.CustomerOrdersModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView.SalesmanModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView.ShopModelAdapter;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.EditDishAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.SalesmanItemsAdapter;
import com.example.android_app_djuraki.dao.FirebaseCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import lombok.var;

public class CustomerOrdersFragment extends Fragment {
    RecyclerView recyclerView;
    private ProgressDialog mDialog = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_customer_orders, null);
        getActivity().setTitle("My orders");
        recyclerView = v.findViewById(R.id.recycle_menu_customer_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mDialog = new ProgressDialog(getContext());
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setMessage("Fetching data...");
        mDialog.show();
        getCustomerOrders();
        return v;
    }

    private void getSalesmanCompanyName(FirebaseCallback callback, String ID) {
        var reference = FirebaseDatabase.getInstance().getReference("salesman").child(ID).child("companyName");
        reference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String name = task.getResult().getValue(String.class);
                callback.onResponse(name);
            } else {
                Log.d("TAG", task.getException().getMessage());
            }
        });
    }

    private void getCustomerOrders() {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("customer").child(userId).child("orders");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                var orders = new ArrayList<CustomerOrderModelAdapter>();
                if(snapshot.hasChildren()) {
                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {
                        CustomerOrderModelAdapter order = new CustomerOrderModelAdapter();
                        order.setOrderID(snapshot1.getKey());
                        order.setIsDelivered(snapshot1.child("isDelivered").getValue(Boolean.class));
                        order.setIsCommented(snapshot1.child("isCommented").getValue(Boolean.class));
                        order.setSalesmanID(snapshot1.child("salesmanID").getValue(String.class));
                        getSalesmanCompanyName(companyName -> {
                            order.setCompanyName(companyName);
                            order.setDateOfOrder(Date.valueOf(snapshot1.child("dateOfOrder").getValue(String.class)));
                            order.setOrderedItems(snapshot1.child("orderedItems").getValue(String.class));
                            orders.add(order);
                            CustomerOrdersModelAdapter s = new CustomerOrdersModelAdapter(getContext(), orders);
                            recyclerView.setAdapter(s);
                            mDialog.dismiss();
                        }, order.getSalesmanID());
                    }
                }
                else{
                    mDialog.dismiss();
                    Toast.makeText(getActivity(),"No orders yet",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}
