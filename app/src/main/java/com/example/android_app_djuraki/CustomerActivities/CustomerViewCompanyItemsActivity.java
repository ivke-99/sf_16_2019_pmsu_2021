package com.example.android_app_djuraki.CustomerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.DishCustomerModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.OrderDishesCustomerModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView.SalesmanModelAdapter;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.SalesmanItemsAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CustomerViewCompanyItemsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<DishCustomerModelAdapter> salesmanItems;
    Button btnGoToCart;
    ArrayList<DishCustomerModelAdapter> selectedItemsForOrder = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_customer_view_company_products);
        setTitle("Companies items");
        recyclerView = findViewById(R.id.recycle_menu_customer_buy_items);
        salesmanItems = getItems();
        String salesmanID = getIntent().getStringExtra("salesmanID");
        if(salesmanItems != null){
            OrderDishesCustomerModelAdapter s = new OrderDishesCustomerModelAdapter(this,salesmanItems);
            recyclerView.setAdapter(s);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            selectedItemsForOrder = s.getOrderedItems();
        }

        btnGoToCart = findViewById(R.id.orderItemsButton);
        btnGoToCart.setOnClickListener(v -> {
            if(!selectedItemsForOrder.isEmpty()) {
                Intent i = new Intent(this, CustomerShoppingCartActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("items", selectedItemsForOrder);
                i.putExtra("bundle", args);
                i.putExtra("salesmanID", salesmanID);
                startActivity(i);
            }
            else{
                Toast.makeText(this,"Please select an item.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<DishCustomerModelAdapter> getItems() {
            Bundle b = getIntent().getBundleExtra("bundle");
            salesmanItems = (ArrayList<DishCustomerModelAdapter>) b.getSerializable("items");
            return salesmanItems;
    }
}