package com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.CustomerActivities.CustomerViewCompanyCommentsActivity;
import com.example.android_app_djuraki.CustomerActivities.CustomerViewCompanyItemsActivity;
import com.example.android_app_djuraki.R;

import java.io.Serializable;
import java.util.List;

public class ShopModelAdapter extends RecyclerView.Adapter<ShopModelAdapter.ViewHolder> {

    private final Context mContext;
    private final List<SalesmanModelAdapter> salesmanModelAdapterList;

    public ShopModelAdapter(Context mContext, List<SalesmanModelAdapter> salesmanModelAdapterList) {
        this.mContext = mContext;
        this.salesmanModelAdapterList = salesmanModelAdapterList;
    }


    @NonNull
    @Override
    public ShopModelAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.customer_menu_companies,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopModelAdapter.ViewHolder holder, int position) {
        final SalesmanModelAdapter salesmanModelAdapter = salesmanModelAdapterList.get(position);
        holder.CompanyName.setText(String.format("Company name: %s", salesmanModelAdapter.getCompanyName()));
        holder.Rating.setText(String.format("Rating: %s", salesmanModelAdapter.getRating()));
        holder.btnViewComments.setOnClickListener(v -> {
            Intent i = new Intent(mContext, CustomerViewCompanyCommentsActivity.class);
            Bundle args = new Bundle();
            args.putSerializable("comments", salesmanModelAdapter.getSalesmanComments());
            i.putExtra("bundle", args);
            mContext.startActivity(i);
        });
        holder.CompanyName.setOnClickListener(v -> {
            Intent i = new Intent(mContext, CustomerViewCompanyItemsActivity.class);
            Bundle args = new Bundle();
            args.putSerializable("items", salesmanModelAdapter.getCompanyProducts());
            i.putExtra("salesmanID", salesmanModelAdapter.getId());
            i.putExtra("bundle", args);
            mContext.startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return salesmanModelAdapterList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView CompanyName,Rating;
        Button btnViewComments;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            CompanyName = itemView.findViewById(R.id.companyNamePicker);
            Rating = itemView.findViewById(R.id.companyRating);
            btnViewComments = itemView.findViewById(R.id.btnViewCompanies);
        }
    }
}
