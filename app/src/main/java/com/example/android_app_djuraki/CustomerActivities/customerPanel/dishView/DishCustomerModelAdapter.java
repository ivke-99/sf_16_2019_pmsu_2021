package com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView;

import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DishCustomerModelAdapter implements Serializable {
    private String id;
    private DishModelAdapter dish;
    private int desiredQuantity;
}
