package com.example.android_app_djuraki.CustomerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.adapters.OrderCreateDTO;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.cartView.ShoppingCartModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.DishCustomerModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.orderView.CustomerOrderModelAdapter;
import com.example.android_app_djuraki.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.val;
import lombok.var;

public class CustomerShoppingCartActivity extends AppCompatActivity {
    DatabaseReference databaseReference;
    RecyclerView recyclerView;
    TextView FinalPrice;
    Button finalizePurchase;
    Double finalPrice = 0.00;
    String salesmanID;
    ArrayList<DishCustomerModelAdapter> selectedItemsForOrder = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_shopping_cart);
        setTitle("Shopping cart");
        recyclerView = findViewById(R.id.recycle_menu_customer_shopping_cart);
        finalizePurchase = findViewById(R.id.btn_finalize_order);
        Bundle b = getIntent().getBundleExtra("bundle");
        selectedItemsForOrder = (ArrayList<DishCustomerModelAdapter>) b.getSerializable("items");
        salesmanID = getIntent().getStringExtra("salesmanID");
        ShoppingCartModelAdapter cartModelAdapter = new ShoppingCartModelAdapter(this,selectedItemsForOrder);
        recyclerView.setAdapter(cartModelAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        FinalPrice = findViewById(R.id.customer_items_quantity);
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("customer").child(userId).child("orders");
        for(DishCustomerModelAdapter model : selectedItemsForOrder) {
            finalPrice += model.getDesiredQuantity() * model.getDish().getPrice();
        }
        FinalPrice.setText(String.format("Final Price: %s", finalPrice));
        finalizePurchase.setOnClickListener(v -> finalizeOrder());
    }

    private void finalizeOrder() {
        ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setMessage("Finalizing order....");
        mDialog.show();
        String key = databaseReference.push().getKey();
        StringBuilder orderedDishes = new StringBuilder();
        for(DishCustomerModelAdapter dish : selectedItemsForOrder) {
            orderedDishes.append(dish.getDish().getDish()).append(" ");
        }
        long millis=System.currentTimeMillis();
        Date date=new Date(millis);
        OrderCreateDTO orderCreateDTO = new OrderCreateDTO(salesmanID,date.toString(),orderedDishes.toString().trim(),false,false);
        databaseReference.child(key).setValue(orderCreateDTO).addOnSuccessListener(unused -> {
            mDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Purchase completed successfully!", Toast.LENGTH_LONG).show();
            Intent i = new Intent(CustomerShoppingCartActivity.this, CustomerMainPage.class);
            startActivity(i);
        });
    }
}

