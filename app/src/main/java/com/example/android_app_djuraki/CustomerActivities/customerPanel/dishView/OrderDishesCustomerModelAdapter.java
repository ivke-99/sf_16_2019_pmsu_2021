package com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.CustomerActivities.CustomerShoppingCartActivity;
import com.example.android_app_djuraki.CustomerActivities.CustomerViewCompanyItemsActivity;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.salesmanView.SalesmanModelAdapter;
import com.example.android_app_djuraki.R;
import com.google.android.material.textfield.TextInputLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OrderDishesCustomerModelAdapter extends RecyclerView.Adapter<OrderDishesCustomerModelAdapter.ViewHolder> {
    private final Context mContext;
    private final List<DishCustomerModelAdapter> dishCustomerModelAdapterList;
    private final ArrayList<DishCustomerModelAdapter> selectedItemsForOrder = new ArrayList<>();

    public OrderDishesCustomerModelAdapter(Context mContext, List<DishCustomerModelAdapter> dishCustomerModelAdapterList) {
        this.mContext = mContext;
        this.dishCustomerModelAdapterList = dishCustomerModelAdapterList;
    }


    @NonNull
    @Override
    public OrderDishesCustomerModelAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.customer_menu_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDishesCustomerModelAdapter.ViewHolder holder, int position) {
        //rework
        final DishCustomerModelAdapter dishCustomerModelAdapter = dishCustomerModelAdapterList.get(position);
        holder.DishName.setText(dishCustomerModelAdapter.getDish().getDish());
        holder.Price.setText(String.format("%s$", dishCustomerModelAdapter.getDish().getPrice()));
        holder.cbSelectItem.setOnClickListener(v -> {
            //2 same orders
            try {
                int quantity = Integer.parseInt(Objects.requireNonNull(holder.Quantity.getEditText()).getText().toString());
                dishCustomerModelAdapter.setDesiredQuantity(quantity);
                selectedItemsForOrder.add(dishCustomerModelAdapter);
                Toast.makeText(mContext,"Added to cart!", Toast.LENGTH_SHORT).show();
            }catch(NumberFormatException n) {
                Toast.makeText(mContext, "Input a quantity", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public ArrayList<DishCustomerModelAdapter> getOrderedItems() {
        return selectedItemsForOrder;
    }

    @Override
    public int getItemCount() {
        return dishCustomerModelAdapterList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView DishName,Price;
        TextInputLayout Quantity;
        Button cbSelectItem;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.customer_dish_image);
            DishName = itemView.findViewById(R.id.customer_dish_name);
            Price = itemView.findViewById(R.id.customer_dish_price);
            cbSelectItem = itemView.findViewById(R.id.cbSelect_item_customer);
            Quantity = itemView.findViewById(R.id.customer_order_quantity);
        }
    }
}
