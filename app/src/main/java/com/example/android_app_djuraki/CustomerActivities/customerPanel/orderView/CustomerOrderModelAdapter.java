package com.example.android_app_djuraki.CustomerActivities.customerPanel.orderView;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerOrderModelAdapter {
    private String orderID;
    private String salesmanID;
    private Date dateOfOrder;
    private String companyName;
    private String orderedItems;
    private Boolean isDelivered;
    private Boolean isCommented;
}
