package com.example.android_app_djuraki.CustomerActivities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.commentView.CommentViewModelAdapter;
import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.DishCustomerModelAdapter;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.CommentModelAdapter;

import java.util.ArrayList;

public class CustomerViewCompanyCommentsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<CommentModelAdapter> salesmanComments;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_view_company_comments);
        setTitle("Company comments");
        recyclerView = findViewById(R.id.recycle_menu_comments);
        salesmanComments = getComments();
        if(salesmanComments != null) {
            CommentViewModelAdapter viewAdapter = new CommentViewModelAdapter(this,salesmanComments);
            recyclerView.setAdapter(viewAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
        else{
            Toast.makeText(this,"No comments yet!", Toast.LENGTH_LONG).show();
        }
    }

    private ArrayList<CommentModelAdapter> getComments() {
        Bundle b = getIntent().getBundleExtra("bundle");
        return (ArrayList<CommentModelAdapter>) b.getSerializable("comments");
    }
}
