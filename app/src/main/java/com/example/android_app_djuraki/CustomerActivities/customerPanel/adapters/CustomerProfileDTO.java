package com.example.android_app_djuraki.CustomerActivities.customerPanel.adapters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerProfileDTO {
    private String address;
    private String fName;
    private String lName;
    private String username;
}
