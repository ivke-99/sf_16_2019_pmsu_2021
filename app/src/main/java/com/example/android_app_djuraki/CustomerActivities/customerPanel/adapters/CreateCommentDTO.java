package com.example.android_app_djuraki.CustomerActivities.customerPanel.adapters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateCommentDTO {
    private Integer rating;
    private String commentText;
    private Boolean isAnonymous;
    private Boolean isArchived = false;
    private String userID;
}
