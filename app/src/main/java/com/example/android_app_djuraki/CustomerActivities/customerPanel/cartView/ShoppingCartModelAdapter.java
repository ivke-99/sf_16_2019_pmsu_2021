package com.example.android_app_djuraki.CustomerActivities.customerPanel.cartView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.CustomerActivities.customerPanel.dishView.DishCustomerModelAdapter;
import com.example.android_app_djuraki.R;

import java.util.List;

public class ShoppingCartModelAdapter extends RecyclerView.Adapter<ShoppingCartModelAdapter.ViewHolder> {

    private final Context mContext;
    private final List<DishCustomerModelAdapter> orderedDishes;

    public ShoppingCartModelAdapter(Context mContext, List<DishCustomerModelAdapter> orderedDishes) {
        this.mContext = mContext;
        this.orderedDishes = orderedDishes;
    }

    @NonNull
    @Override
    public ShoppingCartModelAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.customer_menu_ordered_item,parent,false);
        return new ShoppingCartModelAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingCartModelAdapter.ViewHolder holder, int position) {
        final DishCustomerModelAdapter dishCustomerModelAdapter = orderedDishes.get(position);
        holder.DishName.setText(String.format("Dish name: %s", dishCustomerModelAdapter.getDish().getDish()));
        holder.Quantity.setText(String.format("Desired quantity: %s", dishCustomerModelAdapter.getDesiredQuantity()));
        holder.Price.setText(String.format("%s$", "Price per unit: " + dishCustomerModelAdapter.getDish().getPrice()));
    }


    @Override
    public int getItemCount() {
        return orderedDishes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView DishName,Price,Quantity;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            DishName = itemView.findViewById(R.id.customer_ordered_item_name);
            Price = itemView.findViewById(R.id.customer_ordered_item_price_per_unit);
            Quantity = itemView.findViewById(R.id.customer_ordered_item_quantity);
        }
    }
}
