package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.SalesmanMainPage;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.EditDishAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.SalesmanItemsAdapter;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SalesmanItemsFragment extends Fragment {

    RecyclerView recyclerView;
    private List<EditDishAdapter> dishes = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_salesman_products, null);
        getActivity().setTitle("My products");
        recyclerView = v.findViewById(R.id.recycle_menu_salesman_items);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("salesman").child(userId).child("items");
        ProgressDialog mDialog = new ProgressDialog(getContext());
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setMessage("Loading....");
        mDialog.show();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fetchSalesmanItems();
                mDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
        return v;
    }

    private void fetchSalesmanItems() {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("salesman").child(userId).child("items");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                dishes.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                   DishModelAdapter dish = snapshot1.getValue(DishModelAdapter.class);
                   EditDishAdapter editDishAdapter = new EditDishAdapter(snapshot1.getKey(), dish.getDish(), dish.getDescription(), dish.getPrice());
                   dishes.add(editDishAdapter);
                }
                SalesmanItemsAdapter s = new SalesmanItemsAdapter(getContext(), dishes);
                recyclerView.setAdapter(s);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}

