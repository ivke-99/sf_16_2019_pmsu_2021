package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.discountView.DiscountModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.discountView.SalesmanDiscountsAdapter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class SalesmanDiscountsFragment extends Fragment {

    RecyclerView recyclerView;
    private final List<DiscountModelAdapter> discountModelAdapterList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_salesman_discounts, null);
        getActivity().setTitle("Discounts");
        recyclerView = v.findViewById(R.id.recycle_menu_salesman_discounts);
        DiscountModelAdapter d = new DiscountModelAdapter(20, Date.valueOf("2021-04-12"), Date.valueOf("2021-05-22"), "A nice discount :)");
        DiscountModelAdapter d2 = new DiscountModelAdapter(20, Date.valueOf("2021-04-12"), Date.valueOf("2021-05-22"), "A nice second discount");
        discountModelAdapterList.add(d);
        discountModelAdapterList.add(d2);
        SalesmanDiscountsAdapter s = new SalesmanDiscountsAdapter(getContext(), discountModelAdapterList);
        recyclerView.setAdapter(s);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return v;
    }
}
