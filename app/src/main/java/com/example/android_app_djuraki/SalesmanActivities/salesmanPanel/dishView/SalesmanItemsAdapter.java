package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.SalesmanEditDeleteItemActivity;

import java.util.List;

public class SalesmanItemsAdapter extends RecyclerView.Adapter<SalesmanItemsAdapter.ViewHolder> {

    private Context mContext;
    private List<EditDishAdapter> dishModelAdapterList;

    public SalesmanItemsAdapter(Context mContext, List<EditDishAdapter> dishModelAdapterList) {
        this.dishModelAdapterList = dishModelAdapterList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public SalesmanItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.salesman_menu_dish,parent,false);
        return new SalesmanItemsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesmanItemsAdapter.ViewHolder holder, int position) {
        final EditDishAdapter dishModelAdapter = dishModelAdapterList.get(position);
        holder.DishName.setText(dishModelAdapter.getDish());
        holder.Price.setText(String.valueOf(dishModelAdapter.getPrice()));

        holder.imageView.setOnClickListener(v -> {
            Intent i = new Intent(mContext, SalesmanEditDeleteItemActivity.class);
            EditDishAdapter forEdit = dishModelAdapterList.get(position);
            i.putExtra("id", forEdit.getID());
            i.putExtra("name", forEdit.getDish());
            i.putExtra("price",forEdit.getPrice());
            i.putExtra("desc",forEdit.getDescription());
            mContext.startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return dishModelAdapterList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView DishName,Price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.menu_image);
            DishName = itemView.findViewById(R.id.dish_name);
            Price = itemView.findViewById(R.id.dish_salesman_price);
        }
    }
}
