package com.example.android_app_djuraki.SalesmanActivities.adapters;

import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.CommentModelAdapter;

import org.w3c.dom.Comment;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CommentViewDTO {
    private String id;
    private CommentModelAdapter commentModelAdapter;
}
