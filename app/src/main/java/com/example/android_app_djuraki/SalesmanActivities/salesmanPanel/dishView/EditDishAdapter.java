package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EditDishAdapter {
    private String ID;
    String Dish,Description;
    Double price;
}
