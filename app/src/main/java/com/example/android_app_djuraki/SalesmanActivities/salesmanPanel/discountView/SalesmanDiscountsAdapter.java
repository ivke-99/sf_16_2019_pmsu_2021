package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.discountView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.R;

import java.util.List;

public class SalesmanDiscountsAdapter extends RecyclerView.Adapter<SalesmanDiscountsAdapter.ViewHolder> {

    private final Context mContext;
    private final List<DiscountModelAdapter> discountModelAdapterList;

    public SalesmanDiscountsAdapter(Context mContext, List<DiscountModelAdapter> discountModelAdapterList) {
        this.mContext = mContext;
        this.discountModelAdapterList = discountModelAdapterList;
    }


    @NonNull
    @Override
    public SalesmanDiscountsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.salesman_menu_discount,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesmanDiscountsAdapter.ViewHolder holder, int position) {
        final DiscountModelAdapter discountModelAdapter = discountModelAdapterList.get(position);
        //image
        holder.StartDate.setText(discountModelAdapter.getDiscountStartDate().toString());
        holder.EndDate.setText(discountModelAdapter.getDiscountEndDate().toString());
        holder.Description.setText(discountModelAdapter.getDiscountMessage());
        holder.Percentage.setText(String.format("Percentage:%s%%", discountModelAdapter.getDiscountPercent().toString()));
    }

    @Override
    public int getItemCount() {
        return discountModelAdapterList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView Percentage,StartDate,EndDate,Description;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Percentage = itemView.findViewById(R.id.discountPercentage);
            StartDate = itemView.findViewById(R.id.beginDate);
            EndDate = itemView.findViewById(R.id.endDate);
            Description = itemView.findViewById(R.id.discountText);
        }
    }
}
