package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.adapters.SalesmanProfileDTO;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class SalesmanHomeFragment extends Fragment {

    TextView CompanyName,Email,Owner,Address;
    private ProgressDialog progressDialog = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_salesman_home, container,false);
        Objects.requireNonNull(getActivity()).setTitle("Home");
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null) {
            CompanyName = view.findViewById(R.id.company_name_field);
            Email = view.findViewById(R.id.email_salesman_field);
            Owner = view.findViewById(R.id.fullname_salesman_field);
            Address = view.findViewById(R.id.address_salesman_field);
            getUserInfo();
        }
    }

    private void getUserInfo() {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("salesman").child(userId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                SalesmanProfileDTO salesmanProfileDTO = snapshot.getValue(SalesmanProfileDTO.class);
                CompanyName.setText(salesmanProfileDTO.getCompanyName());
                Email.setText(salesmanProfileDTO.getEmail());
                Owner.setText(String.format("%s %s", salesmanProfileDTO.getFName(), salesmanProfileDTO.getLName()));
                Address.setText(salesmanProfileDTO.getAddress());
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
