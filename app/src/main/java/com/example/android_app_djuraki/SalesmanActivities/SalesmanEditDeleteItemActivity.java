package com.example.android_app_djuraki.SalesmanActivities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.EditDishAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.ktx.Firebase;

public class SalesmanEditDeleteItemActivity extends AppCompatActivity {

    TextInputLayout Name,Desc,Price;
    String name,desc,id;
    Double price;
    Button EditDish,DeleteDish;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_salesman_edit_delete_item);
        setTitle("Product details");
        Name = findViewById(R.id.dishName);
        Desc = findViewById(R.id.description);
        Price = findViewById(R.id.price);
        EditDish = findViewById(R.id.edit_dish);
        DeleteDish = findViewById(R.id.delete_dish);

        getData();
        setData();
        EditDish.setOnClickListener(v -> {
          name = Name.getEditText().getText().toString().trim();
          desc = Desc.getEditText().getText().toString().trim();
          price = Double.valueOf(Price.getEditText().getText().toString().trim());
          if(name.isEmpty() || desc.isEmpty() || price.isNaN() || price == 0) {
              Toast.makeText(this, "Field input error.", Toast.LENGTH_SHORT).show();
          }
          else{
              String salesmanId = FirebaseAuth.getInstance().getCurrentUser().getUid();
              DishModelAdapter newDish = new DishModelAdapter(name,desc,Double.valueOf(price));
              FirebaseDatabase.getInstance().getReference("salesman").child(salesmanId).child("items").child(id).setValue(newDish).addOnCompleteListener(task -> {
                  Toast.makeText(SalesmanEditDeleteItemActivity.this, "Dish Updated Successfully", Toast.LENGTH_SHORT).show();
              });
          }
        });

        DeleteDish.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(SalesmanEditDeleteItemActivity.this);
            builder.setMessage("Are you sure you want to delete this dish?");
            builder.setPositiveButton("YES", (dialog, which) -> {
                FirebaseDatabase.getInstance().getReference("salesman").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("items").child(id).removeValue();
                AlertDialog.Builder food = new AlertDialog.Builder(SalesmanEditDeleteItemActivity.this);
                food.setMessage("Your Dish has been Deleted");
                food.setPositiveButton("OK", (dialog1, which1) -> startActivity(new Intent(SalesmanEditDeleteItemActivity.this, SalesmanMainPage.class)));
                AlertDialog alertt = food.create();
                alertt.show();
            });
            builder.setNegativeButton("NO", (dialog, which) -> dialog.cancel());
            AlertDialog alert = builder.create();
            alert.show();
        });


    }

    private void getData(){
        if(getIntent().hasExtra("price") && getIntent().hasExtra("name") && getIntent().hasExtra("desc") && getIntent().hasExtra("id")) {
            id = getIntent().getStringExtra("id").trim();
            name = getIntent().getStringExtra("name").trim();
            desc = getIntent().getStringExtra("desc").trim();
            price = getIntent().getDoubleExtra("price", 0 );
        }
        else{
            Toast.makeText(this,"No data.",Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        Name.getEditText().setText(name);
        Desc.getEditText().setText(desc);
        Price.getEditText().setText(String.valueOf(price));
    }
}
