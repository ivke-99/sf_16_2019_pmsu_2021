package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.Utils.AlertDialogHelper;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;

public class SalesmanAddDiscountFragment extends Fragment {
    TextInputLayout Percentage,StartingDate,EndingDate,Description;
    Button addDiscount;
    Integer percentage;
    String description;
    Date startingDate, endingDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_salesman_add_discount,null);
        getActivity().setTitle("Add discount");
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            Percentage = view.findViewById(R.id.addDiscountPercentage);
            StartingDate = view.findViewById(R.id.startDate);
            Description = view.findViewById(R.id.discountDescription);
            EndingDate = view.findViewById(R.id.endDate);
            addDiscount = view.findViewById(R.id.postDiscount);
            addDiscount.setOnClickListener(v -> {
                percentage = Integer.parseInt(Percentage.getEditText().getText().toString());
                description = Description.getEditText().getText().toString().trim();
                //startingDate = StartingDate.getEditText().getText();
                //end date
                //for now
                ProgressDialog mDialog = new ProgressDialog(getActivity());
                mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mDialog.setCancelable(false);
                mDialog.setCanceledOnTouchOutside(false);
                mDialog.setMessage("Item is being added.Please wait.");
                mDialog.show();
                new Handler().postDelayed(mDialog::dismiss,3000);

            });
        }
    }


}
