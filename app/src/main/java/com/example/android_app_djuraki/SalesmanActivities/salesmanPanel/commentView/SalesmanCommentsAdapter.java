package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.adapters.CommentViewDTO;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.discountView.SalesmanDiscountsAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SalesmanCommentsAdapter extends RecyclerView.Adapter<SalesmanCommentsAdapter.ViewHolder> {

    private final Context mContext;
    private final List<CommentViewDTO> commentModelAdapterList;

    public SalesmanCommentsAdapter(Context mContext, List<CommentViewDTO> commentModelAdapterList) {
        this.mContext = mContext;
        this.commentModelAdapterList = commentModelAdapterList;
    }

    @NonNull
    @Override
    public SalesmanCommentsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.salesman_menu_comment, parent, false);
        return new SalesmanCommentsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesmanCommentsAdapter.ViewHolder holder, int position) {
        final CommentViewDTO commentModelAdapter = commentModelAdapterList.get(position);
        if (commentModelAdapter.getCommentModelAdapter().getIsAnonymous()) {
            holder.User.setText("Anonymous comment");
        } else {
            holder.User.setText(commentModelAdapter.getCommentModelAdapter().getUserName());
        }

        holder.Rating.setText(String.format("Rating: %s", commentModelAdapter.getCommentModelAdapter().getRating()));
        holder.CommentText.setText(commentModelAdapter.getCommentModelAdapter().getCommentText());
        holder.archiveComment.setOnClickListener(v -> {
            FirebaseDatabase.getInstance().getReference("salesman").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("comments")
                    .child(commentModelAdapter.getId()).child("isArchived").setValue(false).addOnCompleteListener(task -> {
                Toast.makeText(mContext, "Comment archived.", Toast.LENGTH_SHORT).show();
                commentModelAdapterList.remove(commentModelAdapter);
                notifyDataSetChanged();
            });
        });
    }

    @Override
    public int getItemCount() {
        return commentModelAdapterList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView User, Rating, CommentText;
        Button archiveComment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            User = itemView.findViewById(R.id.comment_user);
            Rating = itemView.findViewById(R.id.comment_rating);
            CommentText = itemView.findViewById(R.id.comment_text);
            archiveComment = itemView.findViewById(R.id.btnArchive);
        }
    }
}
