package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CommentModelAdapter implements Serializable {
    private Integer rating;
    private String commentText;
    private Boolean isAnonymous;
    private Boolean isArchived;
    private String userName;
}
