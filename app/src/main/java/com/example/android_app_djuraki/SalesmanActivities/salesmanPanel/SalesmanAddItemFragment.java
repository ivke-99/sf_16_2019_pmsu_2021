package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.DishModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView.EditDishAdapter;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class SalesmanAddItemFragment extends Fragment {

    ImageButton imageButton;
    Button add_dish;
    TextInputLayout Name,Desc,Price;
    String name,desc,price;
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth FAuth;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_salesman_add_item,container, false);
        getActivity().setTitle("Add product");
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        FAuth = FirebaseAuth.getInstance();
        String userId = FAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("salesman").child(userId).child("items");
        if (view != null) {
            // will need to realize a logic for uploading an image
            Name = view.findViewById(R.id.dishName);
            Desc = view.findViewById(R.id.description);
            imageButton = view.findViewById(R.id.imageupload);
            Price = view.findViewById(R.id.price);
            add_dish = view.findViewById(R.id.post);
            add_dish.setOnClickListener(v -> {
                name = Name.getEditText().getText().toString().trim();
                desc = Desc.getEditText().getText().toString().trim();
                price = Price.getEditText().getText().toString().trim();
                if(isValid()) {
                    ProgressDialog mDialog = new ProgressDialog(getActivity());
                    mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mDialog.setCancelable(false);
                    mDialog.setCanceledOnTouchOutside(false);
                    mDialog.setMessage("Item is being added.Please wait.");
                    mDialog.show();
                    DishModelAdapter tempDish = new DishModelAdapter(name,desc,Double.valueOf(price));
                    String key = FirebaseDatabase.getInstance().getReference("salesman").child(userId).child("items").push().getKey();
                    databaseReference.child(key).setValue(tempDish).addOnSuccessListener(aVoid -> {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), "Item is added.", Toast.LENGTH_SHORT).show();
                    });
                }
                else{
                    Toast.makeText(getContext(), "Input error.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean isValid() {
        Name.setErrorEnabled(false);
        Name.setError("");
        Desc.setErrorEnabled(false);
        Desc.setError("");
        Price.setErrorEnabled(false);
        Price.setError("");

        boolean isvalidName=false,isValidDesc=false,isValidPrice=false;
        if (TextUtils.isEmpty(name)) {
            Name.setErrorEnabled(true);
            Name.setError("Name of the dish is required.");
        } else {
            isvalidName = true;
        }

        if (TextUtils.isEmpty(desc)) {
            Desc.setErrorEnabled(true);
            Desc.setError("Description is required");
        } else {
            isValidDesc = true;
        }

        if(TextUtils.isDigitsOnly(price) && !(TextUtils.isEmpty(price))) {
            isValidPrice = true;
        }
        else{
            Price.setErrorEnabled(true);
            Price.setError("Price must be a number and cannot be null.");
        }

        return isvalidName && isValidDesc && isValidPrice;
    }
}
