package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.discountView;

import java.time.LocalDate;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DiscountModelAdapter {
    private Integer discountPercent;
    private Date discountStartDate,discountEndDate;
    private String discountMessage;
}
