package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.adapters.CommentViewDTO;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.CommentModelAdapter;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.commentView.SalesmanCommentsAdapter;
import com.example.android_app_djuraki.dao.FirebaseCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SalesmanCommentsFragment extends Fragment {
    RecyclerView recyclerView;
    private final List<CommentViewDTO> comments = new ArrayList<>();
    private ProgressDialog progressDialog = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_salesman_comments, null);
        getActivity().setTitle("Order comments");
        recyclerView = v.findViewById(R.id.recycle_menu_salesman_comments);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getComments();
        return v;
    }

    private void getComments() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
        FirebaseDatabase.getInstance().getReference("salesman").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("comments").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if (snapshot.hasChildren()) {
                    for (DataSnapshot snapshot2 : snapshot.getChildren()) {
                        if (!snapshot2.child("isArchived").getValue(Boolean.class)) {
                            CommentViewDTO commentViewDTO = new CommentViewDTO();
                            commentViewDTO.setId(snapshot2.getKey());
                            CommentModelAdapter commentModelAdapter = new CommentModelAdapter();
                            commentModelAdapter.setCommentText(snapshot2.child("commentText").getValue(String.class));
                            commentModelAdapter.setIsAnonymous(snapshot2.child("isAnonymous").getValue(Boolean.class));
                            commentModelAdapter.setIsArchived(snapshot2.child("isArchived").getValue(Boolean.class));
                            commentModelAdapter.setRating(snapshot2.child("rating").getValue(Integer.class));
                            String userId = snapshot2.child("userID").getValue(String.class);
                            getUserName(name -> {
                                commentModelAdapter.setUserName(name);
                                commentViewDTO.setCommentModelAdapter(commentModelAdapter);
                                comments.add(commentViewDTO);
                                SalesmanCommentsAdapter s = new SalesmanCommentsAdapter(getContext(), comments);
                                recyclerView.setAdapter(s);
                            }, userId);
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "No comments yet!", Toast.LENGTH_LONG).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
    }

    private void getUserName(FirebaseCallback callback, String userId) {
        FirebaseDatabase.getInstance().getReference("customer").child(userId).child("username").get().addOnCompleteListener(task2 -> {
            if (task2.isSuccessful()) {
                callback.onResponse(task2.getResult().getValue(String.class));
            }
        });
    }
}

