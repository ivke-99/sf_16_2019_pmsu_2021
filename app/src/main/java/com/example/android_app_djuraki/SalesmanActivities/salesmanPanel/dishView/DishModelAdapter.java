package com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.dishView;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DishModelAdapter implements Serializable {
    String Dish,Description;
    Double price;
}
