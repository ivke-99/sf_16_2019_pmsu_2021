package com.example.android_app_djuraki.SalesmanActivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.SalesmanAddDiscountFragment;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.SalesmanAddItemFragment;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.SalesmanCommentsFragment;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.SalesmanDiscountsFragment;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.SalesmanHomeFragment;
import com.example.android_app_djuraki.SalesmanActivities.salesmanPanel.SalesmanItemsFragment;
import com.example.android_app_djuraki.GlobalActivities.MainActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class SalesmanMainPage extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_main_page);
        BottomNavigationView navigationView = findViewById(R.id.salesman_bottom_navigation);
        loadSalesmanFragment(new SalesmanHomeFragment());
        navigationView.setOnNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation_top_salesman, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch(item.getItemId()){
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                break;
            case R.id.salesman_comments:
                fragment = new SalesmanCommentsFragment();
                break;
        }
        return loadSalesmanFragment(fragment);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch(item.getItemId()) {
            case R.id.salesman_home:
                fragment = new SalesmanHomeFragment();
                break;
            case R.id.salesman_items:
                fragment = new SalesmanItemsFragment();
                break;
            case R.id.salesman_actions:
                fragment = new SalesmanDiscountsFragment();
                break;
            case R.id.salesman_add_item:
                fragment = new SalesmanAddItemFragment();
                break;
            case R.id.salesman_add_discount:
                fragment = new SalesmanAddDiscountFragment();
                break;
        }
        return loadSalesmanFragment(fragment);
    }

    public boolean loadSalesmanFragment(Fragment fragment) {
        if(fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
            return true;
        }
        return false;
    }

}