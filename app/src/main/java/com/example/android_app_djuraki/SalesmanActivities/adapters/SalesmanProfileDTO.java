package com.example.android_app_djuraki.SalesmanActivities.adapters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SalesmanProfileDTO {
    private String address;
    private String companyName;
    private String fName;
    private String lName;
    private String email;
}
