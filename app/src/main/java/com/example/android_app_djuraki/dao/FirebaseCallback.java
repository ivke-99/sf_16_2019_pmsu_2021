package com.example.android_app_djuraki.dao;

public interface FirebaseCallback {
    void onResponse(String name);
}
