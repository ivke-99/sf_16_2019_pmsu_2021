package com.example.android_app_djuraki.dao;

public interface FetchInfoCallback {
    void onResponse(String role,Boolean isEnabled);
}
