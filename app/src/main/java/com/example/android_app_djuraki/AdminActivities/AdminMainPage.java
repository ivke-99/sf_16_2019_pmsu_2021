package com.example.android_app_djuraki.AdminActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.AdminActivities.userView.AdminUsersAdapter;
import com.example.android_app_djuraki.AdminActivities.userView.UserModelAdapter;
import com.example.android_app_djuraki.GlobalActivities.MainActivity;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.dao.FetchInfoCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import lombok.Data;
import lombok.var;

public class AdminMainPage extends AppCompatActivity {
    RecyclerView recyclerView;
    private final List<UserModelAdapter> userModelAdapterList = new ArrayList<>();
    public AdminUsersAdapter adminUsersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        recyclerView = findViewById(R.id.recycle_menu_admin_users);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setTitle("Users management");
        ProgressDialog mDialog = new ProgressDialog(AdminMainPage.this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setMessage("Loading...");
        mDialog.show();
        FirebaseDatabase.getInstance().getReference().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                getUsers();
                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    adminUsersAdapter = new AdminUsersAdapter(getApplicationContext(), userModelAdapterList);
                    recyclerView.setAdapter(adminUsersAdapter);
                    mDialog.dismiss();
                }, 3000);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation_top_admin, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        return true;
    }



    private void getUsers() {
        userModelAdapterList.clear();
        FirebaseDatabase.getInstance().getReference("customer").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.hasChildren()) {
                    for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        String key = dataSnapshot.getKey();
                        String username = dataSnapshot.child("username").getValue(String.class);
                        String fullName = dataSnapshot.child("fName").getValue(String.class) + " " + dataSnapshot.child("lName").getValue(String.class);
                        var reference = FirebaseDatabase.getInstance().getReference("user").child(key);
                        reference.get().addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                String role = task.getResult().child("Role").getValue(String.class);
                                String isEnabled = task.getResult().child("Enabled").getValue(String.class);
                                Boolean isEnabled1 = isEnabled.equals("true");
                                UserModelAdapter userModelAdapter = new UserModelAdapter(key, fullName, username, role, isEnabled1);
                                userModelAdapterList.add(userModelAdapter);
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("TAG", "No customers.", error.toException());
            }
        });

        FirebaseDatabase.getInstance().getReference("salesman").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.hasChildren()) {
                    for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        String key1 = dataSnapshot.getKey();
                        String username1 = dataSnapshot.child("username").getValue(String.class);
                        String fullName1 = dataSnapshot.child("fName").getValue(String.class) + " " + dataSnapshot.child("lName").getValue(String.class);
                        var reference = FirebaseDatabase.getInstance().getReference("user").child(key1);
                        reference.get().addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                String role = task.getResult().child("Role").getValue(String.class);
                                String isEnabled = task.getResult().child("Enabled").getValue(String.class);
                                Boolean isEnabled1 = isEnabled.equals("true");
                                UserModelAdapter userModelAdapter = new UserModelAdapter(key1, fullName1, username1, role, isEnabled1);
                                userModelAdapterList.add(userModelAdapter);
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("TAG", "No salesmans.", error.toException());
            }
        });
    }
}
