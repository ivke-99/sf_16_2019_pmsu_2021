package com.example.android_app_djuraki.AdminActivities.userView;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserModelAdapter {
    private String key;
    private String fullName;
    private String userName;
    private String role;
    private Boolean enabled;
}
