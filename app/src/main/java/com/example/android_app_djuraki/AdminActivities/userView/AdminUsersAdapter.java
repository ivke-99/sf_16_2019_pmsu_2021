package com.example.android_app_djuraki.AdminActivities.userView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_app_djuraki.AdminActivities.AdminMainPage;
import com.example.android_app_djuraki.R;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AdminUsersAdapter extends RecyclerView.Adapter<AdminUsersAdapter.ViewHolder> {

    private final Context mContext;
    private final List<UserModelAdapter> userModelAdapterList;

    public AdminUsersAdapter(Context mContext, List<UserModelAdapter> userModelAdapterList) {
        this.mContext = mContext;
        this.userModelAdapterList = userModelAdapterList;
    }

    @NonNull
    @Override
    public AdminUsersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.admin_menu_user, parent, false);
        return new AdminUsersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdminUsersAdapter.ViewHolder holder, int position) {
        final UserModelAdapter userModelAdapter = userModelAdapterList.get(position);
        holder.FullName.setText(userModelAdapter.getFullName());
        holder.Username.setText(userModelAdapter.getUserName());
        holder.Role.setText(userModelAdapter.getRole());
        String key = userModelAdapter.getKey();
        if (userModelAdapter.getEnabled()) {
            holder.btnEnableUser.setEnabled(false);
            holder.btnEnableUser.setVisibility(View.GONE);
            if (!holder.btnDisableUser.isEnabled()) {
                holder.btnDisableUser.setEnabled(true);
                holder.btnDisableUser.setVisibility(View.VISIBLE);
            }
            holder.btnDisableUser.setOnClickListener(v -> FirebaseDatabase.getInstance().getReference("user").child(key).child("Enabled").setValue("false").addOnCompleteListener(task -> {
                Toast.makeText(mContext, "User disabled.", Toast.LENGTH_SHORT).show();
                userModelAdapterList.remove(userModelAdapter);
                userModelAdapter.setEnabled(false);
                userModelAdapterList.add(userModelAdapter);
                notifyDataSetChanged();

            }));
        } else {
            holder.btnDisableUser.setEnabled(false);
            holder.btnDisableUser.setVisibility(View.GONE);
            if (!holder.btnEnableUser.isEnabled()) {
                holder.btnEnableUser.setEnabled(true);
                holder.btnEnableUser.setVisibility(View.VISIBLE);
            }
            holder.btnEnableUser.setOnClickListener(v -> FirebaseDatabase.getInstance().getReference("user").child(key).child("Enabled").setValue("true").addOnCompleteListener(task -> {
                notifyDataSetChanged();
                Toast.makeText(mContext, "User enabled.", Toast.LENGTH_SHORT).show();
                userModelAdapterList.remove(userModelAdapter);
                userModelAdapter.setEnabled(true);
                userModelAdapterList.add(userModelAdapter);
                notifyDataSetChanged();
            }));
        }
    }

    @Override
    public int getItemCount() {
        return userModelAdapterList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView FullName, Username, Role;
        Button btnEnableUser, btnDisableUser;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            FullName = itemView.findViewById(R.id.adminView_user_fullName);
            Username = itemView.findViewById(R.id.adminView_username);
            Role = itemView.findViewById(R.id.adminView_user_type);
            btnEnableUser = itemView.findViewById(R.id.adminView_btnEnableUser);
            btnDisableUser = itemView.findViewById(R.id.adminView_btnDisableUser);
        }
    }
}
