package com.example.android_app_djuraki.GlobalActivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android_app_djuraki.AdminActivities.AdminMainPage;
import com.example.android_app_djuraki.CustomerActivities.CustomerMainPage;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.SalesmanMainPage;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    DatabaseReference databaseReference;
    FirebaseAuth Fauth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);

        new Handler().postDelayed(() -> {
            Fauth = FirebaseAuth.getInstance();
            if (Fauth.getCurrentUser() != null) {
                if (Fauth.getCurrentUser().isEmailVerified()) {
                    Fauth = FirebaseAuth.getInstance();
                    FirebaseDatabase.getInstance().getReference("user").child(FirebaseAuth.getInstance().getUid() + "/Enabled").get().addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            String enabled = task.getResult().getValue(String.class);
                            if(enabled != null) {
                                if (enabled.equals("false")) {
                                    Toast.makeText(MainActivity.this, "Your account has been suspended.", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(MainActivity.this, MainMenuActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                    });
                    databaseReference = FirebaseDatabase.getInstance().getReference("user").child(FirebaseAuth.getInstance().getUid() + "/Role");
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String role = dataSnapshot.getValue(String.class);
                                if(role != null) {
                                    if (role.equals("customer")) {
                                        Intent n = new Intent(MainActivity.this, CustomerMainPage.class);
                                        startActivity(n);
                                        finish();
                                    }
                                    if (role.equals("salesman")) {
                                        Intent a = new Intent(MainActivity.this, SalesmanMainPage.class);
                                        startActivity(a);
                                        finish();
                                    }
                                    if (role.equals("admin")) {
                                        Intent intent = new Intent(MainActivity.this, AdminMainPage.class);
                                        startActivity(intent);
                                    }
                                }
                                else{
                                    Intent intent = new Intent(MainActivity.this, MainMenuActivity.class);
                                    startActivity(intent);
                                }
                            finish();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(MainActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    });
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please verify your account using your e-mail.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", (dialog, which) -> {
                        dialog.dismiss();
                        Intent intent = new Intent(MainActivity.this, MainMenuActivity.class);
                        startActivity(intent);
                        finish();
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                    Fauth.signOut();
                }
            } else {
                Intent intent = new Intent(MainActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }

        }, 3000);
    }
}