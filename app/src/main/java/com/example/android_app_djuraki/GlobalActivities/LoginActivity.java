package com.example.android_app_djuraki.GlobalActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.android_app_djuraki.AdminActivities.AdminMainPage;
import com.example.android_app_djuraki.CustomerActivities.CustomerMainPage;
import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.SalesmanActivities.SalesmanMainPage;
import com.example.android_app_djuraki.dao.FirebaseCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;

import lombok.var;

public class LoginActivity extends AppCompatActivity {
    Button button;
    FirebaseAuth firebaseAuth;
    TextInputLayout email, pass;
    Boolean isSalesman, isCustomer, isAdmin, isEnabled = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = findViewById(R.id.Lemail);
        pass = findViewById(R.id.Lpassword);
        Intent i = getIntent();
        String role = i.getStringExtra("Role").trim();
        firebaseAuth = FirebaseAuth.getInstance();
        button = findViewById(R.id.buttonLogin);
        button.setOnClickListener(v -> {
            String emailtext = email.getEditText().getText().toString().trim();
            String passtext = pass.getEditText().getText().toString().trim();
            if (emailtext.isEmpty() || passtext.isEmpty()) {
                Toast.makeText(this, "Please input your email and password.", Toast.LENGTH_SHORT).show();
            } else {

                final ProgressDialog mDialog = new ProgressDialog(LoginActivity.this);
                mDialog.setCanceledOnTouchOutside(false);
                mDialog.setCancelable(false);
                mDialog.setMessage("Logging in...");
                mDialog.show();
                if (role.equals("Customer")) {
                    firebaseAuth.signInWithEmailAndPassword(emailtext, passtext).addOnCompleteListener(task -> {
                        isCustomer = false;
                        isEnabled = true;
                        if (task.isSuccessful()) {
                            mDialog.dismiss();
                            //check what is user role
                            final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.setCancelable(false);
                            dialog.setMessage("Validating user role...");
                            dialog.show();
                            isUserDisabled(d -> {
                                if (d.equals("false")) {
                                    isEnabled = false;
                                }
                                readFirebaseName(name -> {
                                    if (name.equals("customer")) {
                                        isCustomer = true;
                                    }
                                    if (!isEnabled) {
                                        dialog.dismiss();
                                        Toast.makeText(this, "Your account has been suspended.", Toast.LENGTH_LONG).show();
                                    } else if (firebaseAuth.getCurrentUser().isEmailVerified() && isCustomer) {
                                        Toast.makeText(this, "Login successful.", Toast.LENGTH_SHORT).show();
                                        Intent customerLogin = new Intent(LoginActivity.this, CustomerMainPage.class);
                                        startActivity(customerLogin);
                                    } else {
                                        dialog.dismiss();
                                        Toast.makeText(this, "Wrong role.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            });

                        } else {
                            mDialog.dismiss();
                            Toast.makeText(this, "Wrong credentials.", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (role.equals("Salesman")) {
                    firebaseAuth.signInWithEmailAndPassword(emailtext, passtext).addOnCompleteListener(task -> {
                        isSalesman = false;
                        if (task.isSuccessful()) {
                            mDialog.dismiss();
                            //check what is user role
                            final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.setCancelable(false);
                            dialog.setMessage("Validating user role...");
                            dialog.show();
                            isUserDisabled(d -> {
                                if (d.equals("disabled")) {
                                    isEnabled = false;
                                }
                                readFirebaseName(name -> {
                                    if (name.equals("salesman")) {
                                        isSalesman = true;
                                    }
                                    if (!isEnabled) {
                                        dialog.dismiss();
                                        Toast.makeText(this, "Your account has been suspended.", Toast.LENGTH_LONG).show();
                                    } else if (firebaseAuth.getCurrentUser().isEmailVerified() && isSalesman) {
                                        Toast.makeText(this, "Login successful.", Toast.LENGTH_SHORT).show();
                                        Intent salesman = new Intent(LoginActivity.this, SalesmanMainPage.class);
                                        startActivity(salesman);
                                    } else {
                                        dialog.dismiss();
                                        Toast.makeText(this, "Wrong login selected.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            });

                        } else {
                            mDialog.dismiss();
                            Toast.makeText(this, "Login failed.", Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    firebaseAuth.signInWithEmailAndPassword(emailtext, passtext).addOnCompleteListener(task -> {
                        isAdmin = false;
                        if (task.isSuccessful()) {
                            mDialog.dismiss();
                            //check what is user role
                            final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.setCancelable(false);
                            dialog.setMessage("Validating user role...");
                            dialog.show();
                            readFirebaseName(name -> {
                                if (name.equals("admin")) {
                                    isAdmin = true;
                                }
                                if (isAdmin) {
                                    Toast.makeText(this, "Login successful.", Toast.LENGTH_SHORT).show();
                                    Intent adminLogin = new Intent(LoginActivity.this, AdminMainPage.class);
                                    startActivity(adminLogin);
                                } else {
                                    dialog.dismiss();
                                    Toast.makeText(this, "Login failed.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            mDialog.dismiss();
                            Toast.makeText(this, "Login failed.", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });
    }

    public void isUserDisabled(FirebaseCallback callback) {
        var reference = FirebaseDatabase.getInstance().getReference("user").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Enabled");
        reference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String enabled = task.getResult().getValue(String.class);
                callback.onResponse(enabled);
            } else {
                Log.d("TAG", task.getException().getMessage());
            }
        });
    }

    public void readFirebaseName(FirebaseCallback callback) {
        var reference = FirebaseDatabase.getInstance().getReference("user").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Role");
        reference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String name = task.getResult().getValue(String.class);
                callback.onResponse(name);
            } else {
                Log.d("TAG", task.getException().getMessage());
            }
        });
    }
}