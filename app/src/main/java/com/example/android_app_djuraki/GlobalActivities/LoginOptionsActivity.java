package com.example.android_app_djuraki.GlobalActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.android_app_djuraki.R;
import com.google.firebase.auth.FirebaseAuth;

public class LoginOptionsActivity extends AppCompatActivity {

    Button Customer,Salesman,Admin;
    Intent intent;
    String type;
    String isRegisterOrLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_options);

        intent = getIntent();
        type = intent.getStringExtra("Home").trim();
        isRegisterOrLogin = intent.getStringExtra("Disable");
        if(!(isRegisterOrLogin == null))
            isRegisterOrLogin = isRegisterOrLogin.trim();
        else {
            isRegisterOrLogin = "";
        }


        Salesman = findViewById(R.id.salesman);
        Customer = findViewById(R.id.customer);
        Admin = findViewById(R.id.admin);

        if(isRegisterOrLogin.equals("Admin"))
            Admin.setVisibility(View.INVISIBLE);

        Salesman.setOnClickListener(v -> {
            if(type.equals("Email")) {
                Intent loginEmail = new Intent(LoginOptionsActivity.this, LoginActivity.class);
                loginEmail.putExtra("Role", "Salesman");
                startActivity(loginEmail);
            }
            else{
                Intent register = new Intent(LoginOptionsActivity.this, RegisterActivity.class);
                register.putExtra("role","salesman");
                startActivity(register);
            }
            finish();
        });

        Customer.setOnClickListener(v -> {
            if(type.equals("Email")) {
                Intent loginEmail = new Intent(LoginOptionsActivity.this, LoginActivity.class);
                //temporary intent for switching pages
                loginEmail.putExtra("Role", "Customer");
                startActivity(loginEmail);
            }
            else{
                Intent register = new Intent(LoginOptionsActivity.this, RegisterActivity.class);
                register.putExtra("role","customer");
                startActivity(register);
            }
            finish();
        });

        Admin.setOnClickListener(v -> {
            if(type.equals("Email")) {
                Intent loginEmail = new Intent(LoginOptionsActivity.this, LoginActivity.class);
                loginEmail.putExtra("Role","Admin");
                startActivity(loginEmail);
            }
            finish();
        });
    }
}