package com.example.android_app_djuraki.GlobalActivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.android_app_djuraki.R;
import com.example.android_app_djuraki.Utils.AlertDialogHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.HashMap;

import lombok.var;

public class RegisterActivity extends AppCompatActivity {

    TextInputLayout Fname, Lname, Email, Pass, CPass, Address, CompanyName, Username;
    String fName, lName, email, password, confPass, address, companyName, role, username;
    Button signUp;
    FirebaseAuth FAuth;
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_register);

        Fname = findViewById(R.id.Firstname);
        Lname = findViewById(R.id.Lastname);
        Email = findViewById(R.id.Email);
        Pass = findViewById(R.id.Pwd);
        CPass = findViewById(R.id.Cpass);
        Address = findViewById(R.id.address);
        CompanyName = findViewById(R.id.companyName);
        Username = findViewById(R.id.userName);
        role = getIntent().getStringExtra("role").trim();

        if (role.equals("customer")) {
            CompanyName.setVisibility(View.INVISIBLE);
            CompanyName.setEnabled(false);
        }

        signUp = findViewById(R.id.SignupButton);



        if(role.equals("salesman")){
            databaseReference = FirebaseDatabase.getInstance().getReference("salesman");
        }
        else{
            databaseReference = FirebaseDatabase.getInstance().getReference("customer");
        }
        FAuth = FirebaseAuth.getInstance();

        signUp.setOnClickListener(v -> {
            fName = Fname.getEditText().getText().toString().trim();
            lName = Lname.getEditText().getText().toString().trim();
            email = Email.getEditText().getText().toString().trim();
            password = Pass.getEditText().getText().toString().trim();
            confPass = CPass.getEditText().getText().toString().trim();
            address = Address.getEditText().getText().toString().trim();
            companyName = CompanyName.getEditText().getText().toString().trim();
            username = Username.getEditText().getText().toString().trim();

            if (areAllFieldsValid()) {
                ProgressDialog mDialog = new ProgressDialog(RegisterActivity.this);
                mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mDialog.setCancelable(false);
                mDialog.setCanceledOnTouchOutside(false);
                mDialog.setMessage("Registration in progress.Please wait.");
                mDialog.show();

                FAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        databaseReference = FirebaseDatabase.getInstance().getReference("user").child(user);
                        final HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("Role", role);
                        hashMap.put("Enabled", "true");
                        databaseReference.setValue(hashMap).addOnCompleteListener(task1 -> {
                            HashMap<String, Object> mapa = new HashMap<>();
                            mapa.put("fName", fName);
                            mapa.put("lName", lName);
                            mapa.put("address", address);
                            mapa.put("username", username);
                            mapa.put("email", email);
                            if (role.equals("salesman")) {
                                mapa.put("companyName", companyName);
                                mapa.put("rating", 0);
                                long millis=System.currentTimeMillis();
                                Date date=new Date(millis);
                                mapa.put("registrationDate", date.toString());
                                FirebaseDatabase.getInstance().getReference("salesman").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .setValue(mapa).addOnCompleteListener(a -> {
                                    mDialog.dismiss();
                                    FAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(b -> {
                                        if (b.isSuccessful()) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                            builder.setMessage("Registered successfully,please verify your email");
                                            builder.setCancelable(false);
                                            builder.setPositiveButton("OK", (dialog, which) -> {

                                                dialog.dismiss();
                                                Intent buba = new Intent(RegisterActivity.this, MainMenuActivity.class);
                                                startActivity(buba);

                                            });
                                            AlertDialog alert = builder.create();
                                            alert.show();

                                        } else {
                                            mDialog.dismiss();
                                            AlertDialogHelper.ShowAlert(RegisterActivity.this, "Error", b.getException().getMessage());
                                        }
                                    });
                                });
                            } else if (role.equals("customer")) {
                                firebaseDatabase.getInstance().getReference("customer").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .setValue(mapa).addOnCompleteListener(a -> {
                                    mDialog.dismiss();
                                    FAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(b -> {
                                        if (b.isSuccessful()) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                            builder.setMessage("Registered successfully,please verify your email");
                                            builder.setCancelable(false);
                                            builder.setPositiveButton("OK", (dialog, which) -> {

                                                dialog.dismiss();
                                                Intent buba = new Intent(RegisterActivity.this, MainMenuActivity.class);
                                                startActivity(buba);

                                            });
                                            AlertDialog alert = builder.create();
                                            alert.show();

                                        } else {
                                            mDialog.dismiss();
                                            AlertDialogHelper.ShowAlert(RegisterActivity.this, "Error", b.getException().getMessage());
                                        }
                                    });
                                });
                            }
                        });
                    }else{
                        mDialog.dismiss();
                        Toast.makeText(this, "Email already exists.", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    public boolean areAllFieldsValid() {
        String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        Email.setErrorEnabled(false);
        Email.setError("");
        Fname.setErrorEnabled(false);
        Fname.setError("");
        Lname.setErrorEnabled(false);
        Lname.setError("");
        Pass.setErrorEnabled(false);
        Pass.setError("");
        CPass.setErrorEnabled(false);
        Address.setError("");
        CompanyName.setErrorEnabled(false);
        CompanyName.setError("");

        boolean isValidFName = false, isValidEmail = false, isValidPassword = false, isValidConfirmPassword = false, isValid = false, isValidAdress = false, isValidLname = false, isValidCompanyName = false, isValidUsername = false;
        if (TextUtils.isEmpty(fName)) {
            Fname.setErrorEnabled(true);
            Fname.setError("First name is required");
        } else {
            isValidFName = true;
        }
        if (TextUtils.isEmpty(lName)) {
            Lname.setErrorEnabled(true);
            Lname.setError("Lastname is required");
        } else {
            isValidLname = true;
        }
        if (TextUtils.isEmpty(email)) {
            Email.setErrorEnabled(true);
            Email.setError("Email is required");
        } else {
            if (email.matches(emailpattern)) {
                isValidEmail = true;
            } else {
                Email.setErrorEnabled(true);
                Email.setError("Enter a valid email address");
            }

        }
        if (TextUtils.isEmpty(password)) {
            Pass.setErrorEnabled(true);
            Pass.setError("Password is required");
        } else {
            if (password.length() < 6) {
                Pass.setErrorEnabled(true);
                Pass.setError("Password is too weak");
            } else {
                isValidPassword = true;
            }
        }
        if (TextUtils.isEmpty(confPass)) {
            CPass.setErrorEnabled(true);
            CPass.setError("Confirm Password is required");
        } else {
            if (!password.equals(confPass)) {
                Pass.setErrorEnabled(true);
                Pass.setError("Passwords don't match");
            } else {
                isValidConfirmPassword = true;
            }
        }
        if (TextUtils.isEmpty(address)) {
            Address.setErrorEnabled(true);
            Address.setError("Address is required.");
        } else {
            isValidAdress = true;
        }


        if (TextUtils.isEmpty(companyName)) {
            CompanyName.setErrorEnabled(true);
            CompanyName.setError("Company name is required");
        } else {
            isValidCompanyName = true;
        }

        if(TextUtils.isEmpty(username)) {
            Username.setErrorEnabled(true);
            Username.setError("Username is required.");
        }
        else{
            isValidUsername = true;
        }

        isValid = isValidFName && isValidLname && isValidEmail && isValidPassword && isValidConfirmPassword && isValidAdress && isValidUsername;
        if (role.equals("salesman"))
            isValid = isValid && isValidCompanyName;
        return isValid;
    }

}