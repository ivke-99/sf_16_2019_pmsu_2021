package com.example.android_app_djuraki.GlobalActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.android_app_djuraki.R;

public class MainMenuActivity extends AppCompatActivity {

    Button signInButton, signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        signInButton = findViewById(R.id.SignwithEmail);
        signUp = findViewById(R.id.SignUp);

        signInButton.setOnClickListener(v -> {
            Intent signIn = new Intent(MainMenuActivity.this, LoginOptionsActivity.class);
            signIn.putExtra("Home", "Email");
            startActivity(signIn);
            finish();
        });

        signUp.setOnClickListener(v -> {
            Intent signUp = new Intent(MainMenuActivity.this, LoginOptionsActivity.class);
            signUp.putExtra("Home", "SignUp");
            signUp.putExtra("Disable","Admin");
            startActivity(signUp);
            finish();
        });
    }
}